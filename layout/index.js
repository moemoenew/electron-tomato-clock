var btnStart = document.getElementById('btnStart')
var btnBreak = document.getElementById('btnBreak')
var spanMin = document.getElementById('munites')
var spanSec = document.getElementById('seconds')
var tomatoTimer

btnStart.addEventListener('click', function(e) {
    let totalSec = 1499
    let tick = 0
    tomatoTimer= setInterval(() => {
        var min = Math.floor((totalSec - tick) / 60)
        spanMin.innerHTML = min.toString().padStart(2, '0')
        spanSec.innerHTML = (totalSec - (min * 60) - tick).toString().padStart(2, '0')
        tick ++
    }, 1000)
})

btnBreak.addEventListener('click', function(e) {
    clearInterval(tomatoTimer)
})