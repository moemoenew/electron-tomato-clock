const { app, BrowserWindow } = require('electron')
const path = require('path')
const url = require('url')

app.on('ready', () => {
    let mainWindow = new BrowserWindow({
        width: 800,
        height: 600
    }) 

    mainWindow.loadFile(path.join(__dirname, './layout/index.html'))

    /* Open dev tool window or not, you could also use cmd+opt+i on mac OS to toogle it */
    //mainWindow.openDevTools()
})